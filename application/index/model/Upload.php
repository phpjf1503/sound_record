<?php
namespace app\index\model;
use think\Model;

class Upload extends Model{

    /**TODO 简单上传单个文件
     * @param $type
     * @param string $filename
     * @param bool $is_water
     * @return array
     */
    public function upfile($type,$filename = 'file',$is_water = false){
        $file = request()->file($filename);
        // 移动到框架应用根目录/uploads/
        $info = $file->move('uploads/'  . $type);
        if($info){
            //获取文件路径
            $path = '/uploads'.'/' . $type ."/".$info->getSaveName();
            $path=str_replace("\\","/",$path);
            return array('code'=>200,'msg'=>'上传成功','path'=>$path,'savename'=>$info->getSaveName(),'filename'=>$info->getFilename(),'info'=>$info->getInfo());
        }else{
            return array('code'=>0,'msg'=>$file->getError());
        }
    }
}
