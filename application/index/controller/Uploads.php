<?php
namespace app\index\controller;
use app\index\model\Upload;
use think\Controller;

class Uploads extends Controller{

    private $uploadModel;
    public function __construct()
    {
        parent::__construct();
        $this->uploadModel = new Upload();
    }
    /**TODO 上传文件
    * @return false|string
    */
    public function upfile(){
        return json_encode($this->uploadModel->upfile('files'));
    }
}
