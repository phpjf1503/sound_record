<?php
namespace app\index\controller;
Vendor("alibabacloud.client.src.AlibabaCloud");
use AlibabaCloud\Client\AlibabaCloud;
use app\common\lib\NLSFileTrans;
use think\Controller;
use think\Request;
use think\response\Json;

class Index extends Controller
{
    private $accessKeyId;
    private $accessKeySecret;
    private $appKey;
   function __construct(Request $request = null)
   {
       parent::__construct($request);
       $this->accessKeyId = config("ali_accessKeyId");
       $this->accessKeySecret = config("ali_accessKeySecret");
       $this->appKey = config("ali_appKey");
   }

    public function index(){

       //这个必须是线上的
       //$fileLink = "https://aliyun-nls.oss-cn-hangzhou.aliyuncs.com/asr/fileASR/examples/nls-sample-16k.wav";
       $fileLink = input("param.fileLink","");//要识别的线上服务器录音文件地址
        if(!$fileLink){
            return APIresponse(0,"录音文件地址为空请重试~",[]);
        }
        /**
        * 第一步：设置一个全局客户端
        * 使用阿里云RAM账号的AccessKey ID和AccessKey Secret进行鉴权
        */
       AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
           ->regionId("cn-shanghai")
           ->asGlobalClient();
       $fileTrans = new NLSFileTrans();
       /**
        *  第二步：提交录音文件识别请求，获取任务ID，用于后续的识别结果轮询
       */
       $taskId = $fileTrans->submitFileTransRequest($this->appKey, $fileLink);
       if(!$taskId){
           return APIresponse(0,"录音文件识别接口请求失败!",[]);
       }
       /**
        * 第三步：根据任务ID轮询识别结果
        */
       $result = $fileTrans->getFileTransResult($taskId);
       if($result){
            //只有$result不是个空对象则可以转成数组
            $result = $result->toArray();//将对象转成数组才可以进行json化处理
            return APIresponse(200,"录音文件识别成功!",$result);
       }else{
            return APIresponse(200,"录音文件识别失败!",[]);
       }
   }
}
