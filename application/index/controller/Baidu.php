<?php
namespace app\index\controller;
use think\Cache;
use think\Controller;
use app\common\lib\AipSpeech;
use think\Request;

/**todo 百度短语音识别极速版API
 * Class Baidu
 * @package app\index\controller
 */
class Baidu extends Controller{
    private $APP_ID;
    private $API_KEY;
    private $SECRET_KEY;
    private $cuid;
    private $url;

    function __construct(Request $request = null)
    {
        parent::__construct($request);
        $this->APP_ID = "21149438";
        $this->API_KEY = "kRdHojmytrdQS6dDEcR3W3EV";
        $this->SECRET_KEY = "a3IShKq0ganvPWdMv5x6ZLE098PwsUOv";
        $this->cuid = "27132215991594015648";//可以随机生成 也可以从前端获取
        $this->url = "https://vop.baidu.com/pro_api";
    }

    function getToken(){
        //$httpUrl = "https://openapi.baidu.com/oauth/2.0/token";//鉴权调用地址
        $auth_url = "https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=".$this->API_KEY."&client_secret=".$this->SECRET_KEY;
        $token = $this->auth_curl($auth_url);
        if($token){
            return $token;
        }else{
            return false;
        }
    }

    function index()
    {
        // 初始化AipSpeech对象
        $aipSpeech = new AipSpeech($this->APP_ID, $this->API_KEY, $this->SECRET_KEY);
        /*$response = $aipSpeech->asr(file_get_contents('https://aliyun-nls.oss-cn-hangzhou.aliyuncs.com/asr/fileASR/examples/nls-sample-16k.wav'), 'wav', 16000, array(
            'lan' => 'zh',
        ));*/
        /*$response = $aipSpeech->asr(file_get_contents('
        ));http://sound.huangmaochang.cn/uploads/files/20200706/7d9610076e5e74a4b88dde6bc806d83d.aac'), 'pcm', 16000, array(
            'lan' => 'zh',
        return APIresponse(200,"识别成功~",$response);*/
        //token的有效期为一个月 缓存
        //获取客户端请求参数音频文件地址
        $fileUrl = input("post.file_url","");//音频线上地址/uplodas/files
        if(!$fileUrl){
            return APIresponse(0,"音频文件地址不存在请检查~",[]);
        }
        if(Cache::get("token")){
            $token = Cache::get("token");
        }else{
            //获取到token
            $token = $this->getToken();
            Cache::set("token",$token,2592000);
        }
        if(!$token){
            return APIresponse(0,"获取token失败~",[]);
        }
        $fileLink = $_SERVER["DOCUMENT_ROOT"].$fileUrl;//将线上化为本地地址
        $audio = file_get_contents($fileLink);//可以支持本地
        $base_data = base64_encode($audio);
        $array = array(
            "format" => "m4a",//音频文件格式
            "rate" => 16000,//采样率
            "channel" => 1,//声道数，仅支持单声道，请填写固定值 1
            //"lan" => "zh",//默认中文（zh）。 中文=zh、粤语=ct、英文=en，不区分大小写。
            "token" => $token,//鉴权用的
            "cuid"=> $this->cuid,//用户唯一标识，用来区分用户，计算UV值。建议填写能区分用户的机器 MAC 地址或 IMEI 码，长度为60字符以内。
            "dev_pid" => 80001,
            "speech" => $base_data,//本地语音文件的的二进制语音数据 ，需要进行base64 编码。与len参数连一起使用。
            "len" => filesize($fileLink),//本地语音文件的的字节数，单位字节
        );
        $json_array = json_encode($array);
        $content_len = "Content-Length: ".strlen($json_array);
        $header = array($content_len, 'Content-Type: application/json; charset=utf-8');
        $response = $this->http_url($this->url,$json_array,$header);
        if($response and $response["err_no"] == 0){//识别成功
            return APIresponse(200,"识别成功~",$response);
        }else{
            return APIresponse(0,"识别失败~",$response);
        }

    }
    /**
     * curl请求 获取token get请求
     */
    function auth_curl($auth_url = ""){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$auth_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //忽略https证书的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response,true);
        if($response and $response["access_token"]){
            return $response["access_token"];
        }else{
            return false;
        }
    }
    /**
     * curl请求 获取识别结果 post请求
     */
    function http_url($url,$json_array,$header){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_array);
        //忽略https证书的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);
        return $response;
    }
}
