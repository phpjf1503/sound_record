<?php
namespace app\common\lib\exception;
use think\exception\Handle;

/**todo 继承Handle异常处理类 重写错误render方法
 * Class ApiHandleException
 * @package app\common\lib\exception
 */
class ApiHandleException extends  Handle {

    /**
     * http 状态码
     * @var int
     */
    public $httpCode = 500;

    public function render(\Exception $e) {

        if(config('app_debug') == true) {
            return parent::render($e);
        }
        if ($e instanceof ApiException) {
            $this->httpCode = $e->httpCode;
        }
        return  APIresponse(0, $e->getMessage(), [], $this->httpCode);
    }
}