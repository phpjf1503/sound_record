<?php
namespace app\common\lib\oss;


use app\common\lib\exception\ApiHandleException;
use OSS\Core\OssException;
use OSS\OssClient;
if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
if (is_file(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

/**阿里云OSS对象存储类
 * Class ALoss
 * @package app\common\oss
 */
class ALoss {
    private $AccessKey; //key
    private $AccessKeySecret;//密钥
    private $Endpoint;
    private $Bucket;
    private $AliyunOSS;
    /**ALoss构造函数
     * ALoss constructor.
     * @throws \OSS\Core\OssException
     */
    public function __construct()
    {
        $this->AccessKey = config('oss.aliyun_oss')["AccessKey"];
        $this->AccessKeySecret = config('oss.aliyun_oss')["AccessKeySecret"];
        $this->Bucket = config('oss.aliyun_oss')["Bucket"];
        $this->Endpoint = config('oss.aliyun_oss')["Endpoint"];
    }

    /**获取ALoss实例对象
     * @param array $options
     * @return ALoss
     */
    public static function instance($options = [])
    {
        return new ALoss($options);
    }

    /**实例化OSS对象
     * @throws \OSS\Core\OssException
     */
    public function createOssClient(){

        try{
            $this->AliyunOSS= new OssClient($this->AccessKey,$this->AccessKeySecret,$this->Endpoint,false);
            // 设置Socket层传输数据的超时时间，单位秒，默认5184000秒。
            $this->AliyunOSS->setTimeout(3600);
            // 设置建立连接的超时时间，单位秒，默认10秒。
            $this->AliyunOSS->setConnectTimeout(10);
        }catch (\Exception $e){
            return APIresponse(0,$e->getMessage(),[]);
        }
        //return ($this->AliyunOSS->getBucketLocation($this->Bucket));
    }

    /**简单上传文件函数
     * @param string $fileName 上传的文件名称
     * @param string $filePath 上传的文件地址
     * 注意: 由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt
     * @return \think\response\Json
     */
    public function simpleUploadFile($fileName='',$filePath='')
    {
        //必须要先实例化OSS对象
        $this->createOssClient();
        if (!$fileName || !$filePath) {
            return APIresponse(0, "文件名或文件地址不存在~", []);
        }
        try {
            $responseData = $this->AliyunOSS->uploadFile($this->Bucket, $fileName, $filePath);
        } catch (\Exception $e) {
            return APIresponse(0, $e->getMessage(), []);
        }
        return $responseData;

    }
}
